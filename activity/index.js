/*Instructions:

	Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.

	Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.

	Create function which will be able to multiply two numbers.
		-Numbers must be provided as arguments.
		-Return the result of the multiplication.

	-Create a new variable called product.
		-This product should be able to receive the result of multiplication function.

	Log the value of product variable in the console.

*/

// A & B
function show(n1,n2){
	
	console.log("The sum is: ");
	console.log(n1 + n2);
	console.log("The difference is: ");
	console.log(n1 - n2);
	
};

let num1= 31;
let num2= 2;
show(num1, num2);


//C. & D

function mul(x,y){
return x * y ;
}

let product = mul(10,2);
console.log("The product is: ");
console.log(product);